using System.Threading.Tasks;

namespace Broker
{
    public interface IUser
    {
        Task ShowUser(User user);
    }
}