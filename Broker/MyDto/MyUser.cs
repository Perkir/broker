using System;
using System.Threading.Tasks;

namespace Broker
{
    public class MyUser :IUser
    {
        public async Task ShowUser(User user)
        {
            if (user.Email != "")
            {
                await Console.Out.WriteLineAsync($"Name: {user.Name} Email: {user.Email} Age: {user.Age}");
            }
            else
            {
                await Console.Out.WriteLineAsync("Email не заполнен");
            }
        }
    }
}