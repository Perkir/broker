using System;
using System.Threading.Tasks;
using MassTransit;

namespace Broker
{
    public class UserConsumer: IConsumer<User>
    {
        private readonly IUser _user;
        
        public UserConsumer(IUser user)
        {
            _user = user;
        }

        public Task Consume(ConsumeContext<User> context)
        {
            return Task.Run(async () =>
            {
                await Console.Out.WriteLineAsync("Получаем сообщение . . .");
                await _user.ShowUser(context.Message);
            });
        }
    }
}