using System;
using System.Threading;
using System.Threading.Tasks;
using Broker.Producer;
using MassTransit;
using Microsoft.Extensions.Hosting;

namespace Broker
{
    public class Service : IHostedService
    {
        private readonly IBusControl _busControl;

        public Service(IBusControl busControl)
        {
            _busControl = busControl;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Сервис запущен...");
            int id = 0;
            while (true)
            {
                Thread.Sleep(1000);
                var user = UserGenerator.Generate(id);
                Console.WriteLine($"Отправили {user.Name} , {user.Email}");
                await _busControl.Publish((user));
                await _busControl.StartAsync(cancellationToken);
                id++;
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Сервис остановлен...");
            await _busControl.StopAsync(cancellationToken);
        }
    }
}