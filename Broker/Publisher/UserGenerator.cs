using System;
using Bogus;

namespace Broker.Producer
{
    public class UserGenerator
    {
        public static User Generate(int id)
        {
            var random = new Random();            
            var userFaker = new Faker<User>()
                .CustomInstantiator(f => new User())
                .RuleFor(u => u.Name, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => id % 2 == 0 ? "" : f.Internet.Email(u.Name))
                .RuleFor(u => u.Age, (f, u) => random.Next(1,100));

            return userFaker.Generate();

        }
    }
}