﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Broker
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var hostBuilder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddMassTransit(c =>
                    {
                        c.AddConsumer<UserConsumer>();
                        c.AddBus(provider => Bus.Factory.CreateUsingInMemory(cfg =>
                       /* c.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                        {
                            var host = cfg.Host(new Uri("rabbitmq://kangaroo-01.rmq.cloudamqp.com:5671/xqnjzyzz/"), h =>
                            {
                                h.Username("xqnjzyzz");
                                h.Password("PyysALoR_GGN7xW3Gxy4ZMd5_Q2IvGRg");
                                h.UseSsl(s =>
                                {
                                    s.Protocol = SslProtocols.Tls12;
                                });
                            });
                           */ 
                            cfg.ReceiveEndpoint( "Homework", e =>
                            {
                                e.ConfigureConsumer<UserConsumer>(provider);
                            })));
                        

                        services.AddScoped<IHostedService, Service>();
                        services.AddScoped<IUser, MyUser>();

                    });
                });

            await hostBuilder.RunConsoleAsync();
        }
    }
}
